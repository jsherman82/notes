# Pipewire

## Disable output device

#### Determine the name of the audio device:

`pw-cli dump |grep node.name`

#### Create a config file to disable the device:

Create: ~/.config/wireplumber/main.lua.d/51-alsa-disable.lua

Place the following into the file (adjust the device.name value):


rule = {
  matches = {
    {
      { "device.name", "equals", "alsa_output.pci-0000_21_00.1.hdmi-stereo-extra1" },
    },
  },
  apply_properties = {
    ["device.disabled"] = true,
  },
}

table.insert(alsa_monitor.rules,rule)
