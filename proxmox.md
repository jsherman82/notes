# Proxmox

## Image creation steps
Steps I used recently with Ubuntu 22.04 for a working template complete with a functional cloud-init:

enable qemu agent in vm settings

provision

sudo apt install qemu-guest-agent

sudo rm /var/lib/dbus/machine-id

ln -s /etc/machine-id /var/lib/dbus/machine-id

truncate -s 0 /etc/machine-id

sudo mv /etc/netplan/00-installer-config.yaml /etc/netplan/network-config.yaml

sudo rm /etc/cloud/cloud.cfg.d/99-installer.cfg

sudo rm /etc/cloud/cloud.cfg.d/subiquity-disable-cloudinit-networking.cfg

cloud-init clean

poweroff

add cloud drive

regen cloud drive

## Useful pct commands

#### List snapshots
`pct listsnapshot <id>`

#### Delete a snapshot
`pct delsnapshot <id>`

#### Remove node from cluster
Change quorum to 1:
`pvecm expected 1`

Remove the node:
`pvecm delnode <nodename>`

Remove the directory for the node (ensures it's removed from GUI):
`rm -r /etc/pve/nodes/<node_name>`

remove keys from `/etc/pve/priv/authorized_keys` and `/root/.ssh/authorized_keys` (if it exists)

## Checklist for generating a new Ubuntu template
The following steps can be completed for creating a new template.

* Install Ubuntu
* Remove ISO image from VM
* Shut down an set CPU/RAM to 1 and 512MB accordingly
* `sudo apt update`
* `sudo apt dist-upgrade`
* `sudo apt autoremove`
* `sudo apt clean`
* Enable qemu-agent in VM options
* `sudo apt install qemu-guest-agent`
* `wget https://raw.githubusercontent.com/RPi-Distro/raspberrypi-sys-mods/master/debian/raspberrypi-sys-mods.regenerate_ssh_host_keys.service`
* `mv raspberrypi-sys-mods.regenerate_ssh_host_keys.service /etc/systemd/system/regenerate_ssh_host_keys.service`
* `systemctl daemon-reload`
* `systemctl enable regenerate_ssh_host_keys.service`
* `find /var/log -type f -exec truncate -s 0 {} \;`
* `truncate -s 0 /etc/machine-id`
* `history -c`
* `history -w`
* shut down
* Create template

## Add enterprise repository (not tested)
From Maxwell (@gotmax831) you can add a non-subscription repo. He stated:

"Just so you know, there is actually a non-subscription repo for Proxmox that you can add in place of the default one. Admittedly, it is not well-documented, but without enabling it the Proxmox packages don't upgrade."

He said he ran this to get it working:
`mv /etc/apt/sources.list.d/pve-enterprise.list{,.disabled}` and then `echo "deb http://download.proxmox.com/debian/pve buster pve-no-subscription" | sudo tee -a /etc/apt/sources.list.d/pve-no-subscription.list`.

## Potential fix for blank console window
qm set 200 --serial0 socket --vga serial0
From:
https://www.kkovach.com/proxmox-cloud-images-an-easier-way-to-build-vms/

## Useful articles

#### ZFS memory adjustment tips:
https://dlford.io/memory-tuning-proxmox-zfs/
