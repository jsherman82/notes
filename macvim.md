# macvim

#### Fix black line issue:
`$ defaults write org.vim.MacVim MMUseCGLayerAlways -bool YES`

#### .gvimrc tweaks:
`set guifont=Menlo\ Regular:h14`

`set lines=35 columns=120`
