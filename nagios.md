# nagios

#### Debian package procedure (tested on Ubuntu 20.04) ######
`sudo apt install nagios4`

`sudo a2enmod authz_groupfile auth_digest`

Enable authentication in `/etc/nagios4/cgi.conf`

Find and replace `nagiosadmin` with `<desired_nagios_user_name>` in `/etc/nagios4/cgi.conf`

    htdigest -c /etc/nagios4/htpasswd.users Nagios4 '<desired_nagios_user_name>'

`sudo systemctl restart apache2`

`sudo systemctl restart nagios4`

`sudo pip3 install twx.botapi`

Note: Will need the required python script for telegram notifications to be copied into `/usr/local/bin`

#### Source install procedure (tested on Ubuntu 18.04) ######
Note: Provision Ansible on the host LAST (Ansible creates a nagios user when it does the nrpe config)

`apt install gcc make binutils cpp libpq-dev libmysqlclient-dev libssl1.0.0 libssl-dev pkg-config libgd-tools perl libperl-dev libnet-snmp-perl snmp apache2 libapache2-mod-php unzip`

Raspbian:
`apt install gcc make binutils cpp libpq-dev libmariadb-dev libssl1.0.2 libssl-dev pkg-config libgd-tools perl libperl-dev libnet-snmp-perl snmp apache2 libapache2-mod-php unzip`

`a2enmod authz_groupfile cgi`

`mkdir /usr/src/nagios4`

`cd /usr/src/nagios4`

Download nagios and plugins source files to `/usr/src/nagios4`

`tar –xvf /path/to/nagios-xx.tar.gz`

`tar –xvf /path/to/nagios-plugins-xx.tar.gz`

`groupadd nagios`

`groupadd nagioscmd`

`useradd -g nagios -G nagioscmd -d /opt/nagios nagios`

`usermod -aG nagioscmd www-data`

`mkdir -p /opt/nagios /opt/nagios/share /etc/nagios /var/nagios`

`chown nagios:nagios -R /opt/nagios /etc/nagios /var/nagios`

cd into extracted nagios directory

`sh configure --prefix=/opt/nagios --sysconfdir=/etc/nagios --localstatedir=/var/nagios --libexecdir=/opt/nagios/plugins --with-command-group=nagioscmd`

`chown nagios:nagioscmd -R /var/nagios/`

`make all`

`cd ./base && make`

`make install`

cd back to root of extracted nagios directory

`make install-commandmode`

`make install-config`

`cd plugindir`

`sh configure --prefix=/opt/nagios --sysconfdir=/etc/nagios --localstatedir=/var/nagios --libexecdir=/opt/nagios/plugins`

`make all`

`make install`

Clone the git repo

Copy startup file from git repository to /etc/init.d/nagios

`update-rc.d nagios defaults`

`mkdir -p /var/nagios/spool/checkresults`

`chown nagios: /var/nagios/spool/checkresults`

Copy apache config file from git repository to `/etc/apache2/conf-available/nagios.conf`

`a2enconf nagios`

`systemctl restart apache2`

`/etc/init.d/nagios restart`

Final steps:

Copy check_nrpe to /opt/nagios/plugins/check_nrpe

Add these lines to sudoers:

    nagios ALL=(ALL) NOPASSWD: /bin/journalctl
    nagios ALL=(ALL) NOPASSWD: /etc/init.d/apache2
    nagios ALL=(ALL) NOPASSWD: /etc/init.d/nagios
    nagios ALL=(ALL) NOPASSWD: /etc/init.d/nagios
For Telegram alerter:

`sudo pip install twx.botapi`

To make `/` redirect to `/nagios`, add this to `000-default.conf`:

`RedirectMatch ^/$ /nagios`