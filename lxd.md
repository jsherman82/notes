# Linux Containers (lxc2)

## Getting started

#### Initialization (new installation)
`lxd init`

#### List available images
`lxc image list images:`

#### List containers
`lxc list'

#### Create a new Ubuntu container
`lxc launch ubuntu:16.04 mycontainer`

or:

`lxc launch images:ubuntu/xenial mycontainer`

#### Create a new Ubuntu container (on a specific storage pool)
`lxc launch ubuntu:16.04 mycontainer -s main`

#### Create a container that matches a specific cloud instance type
`lxc launch -t m3.large ubuntu:16.04 aws-m3large`

#### Delete a container
`lxc delete mycontainer`

#### Running a command within a container
`lxc exec <container_name> bash`

#### Starting a container
`lxc-start mycontainer`

#### Stopping a container
`lxc-stop mycontainer` (verify this, may work as of lxc 3.0)

#### Login in to the container
`lxc exec mycontainer -- sudo --login --user ubuntu`

#### Running a command which requires arguments within a container
`lxc exec <container_name> -- ls -lh`

#### Set CPU limit on container
`lxc config set mycontainer limits.cpu 3`

## Snapshots

#### Take a snapshot
`lxc snapshot [container] [snapshot_name]`

#### List snapshots of a container
`lxc info [container]` (snapshots are shown in the output)

#### Restore a snapshot
`lxc restore [container] [snapshot_name]`

#### Delete a snapshot
`lxc delete [container]/[snapshot_name]`

## Working with profiles
#### Create a new profile
`lxc profile create myprofile`

#### Edit a profile
`lxc profile edit myprofile`

#### Delete a profile
`lxc profile delete myprofile1`

#### Launch a container with a specific profile:
`lxc launch mynewcontainer -p myprofile -p extbr0`

#### Launch a container with a specific profile, and apply a second to override the first:
`lxc launch mynewcontainer -p default -p extbr0`

#### Show the current profile configuration
`lxc profile show extbr0`

#### Edit a network profile
`lxc profile edit extbr0`

#### External accessible bridge example
    description: External access profile
    devices:
      eth0:
        name: eth0
        nictype: bridged
        parent: br0
        type: nic

## Setting resource limits
#### Limit disk space:
Note: From what I understand, if disk size is inherited by profile, this will not work

`lxc config device set mycontainer root size 20GB`

#### Limit RAM:
`lxc config set mycontiner limits.memory 1GB`

## Mounting host directories

#### Mount a directory from the host
`lxc config device add container-name device-name disk source=/path/from/host path=/path/on/container`

#### Remove a mounted directory from the host
`lxc config device remove container-name device-name`

## Working with storage pools

#### List storage pools
`lxc storage list`

#### Creating a new storage pool, mapped to file-system directory
`lxc storage create main dir source=/opt/lxd`

#### Create a new storage pool, mapped to block device
`lxc storage create pool-name btrfs source=/dev/sdb`

#### Deleting a storage pool
`lxc storage delete main`

###### Set default storage pool
`lxc profile edit default`

Set the default storage volume in the editor that appears

#### Working with images

#### List image repositories
`lxc image list`

### List available images
`lxc image list`

### Retrieve information about an image
`lxc image info <image_name>`

#### Add a new image repository
`lxc remote add <name> <host>`

### Delete a local image
`lxc image delete <image_name>`

#### Create an image from an existing container
`lxc publish <container> --alias <new_image_name>`

## Configuration

#### Automatically start a container at boot
`lxc config set <container_name> boot.autostart 1`

#### Delay autostart of a container
`lxc config set <container_name> boot.autostart.delay 30`

#### Set priority (setting autostart order)
`lxc config set <container_name> boot.autostart.order 8`

## Working with remotes
#### Show remote servers
`lxc remote list`

#### Add a remote
On the new server:

  `lxc config set core.https_address [::]:8443`

  `lxc config set core.trust_password something-secure`

On the existing server:

  `lxc config set core.https_address [::]:8443`

  `lxc remote add foo 1.2.3.4`

#### Start a container on the remote
`lxc launch ubuntu:18.04 server:c1`

#### Copy a container to a remote
`lxc copy mycontainer server:mycontainer`

#### Move a container to a remote
`move move mycontainer server:mycontainer`

## Previous issues
#### Unable to nest Docker container inside LXD container
Launch the contianer with the following options:

`lxc launch ubuntu:18.04 testoffice -c security.privileged=true -c security.nesting=true`

## Helpful links
https://www.jamescoyle.net/cheat-sheets/2540-lxc-2-x-lxd-cheat-sheet