# cloud-init

## Examples

#### Clear cloud-init data (force it to re-run)
    sudo rm -rf /var/lib/cloud/*
Note: Clearing the contents of `/etc/machine-id` may (or may not) also be necessary
