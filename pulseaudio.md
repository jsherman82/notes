# Pulseaudio

## Stop the clicking sound (suspend audio card)

Note: Not tested, mentioned by someone in a YouTube comment in the Serval review

Edit `/etc/pulse/default.pa` and comment out `load-module module-suspend-on-idle`