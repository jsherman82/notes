# Multipass

## Installation

TBC

## Commands

#### Launch an instance (creates LTS instance by default)
  `multipass launch --name my-ubuntu-instance`

#### List multipass instances
  `multipass list`

#### Delete an instance
  `multipass delete my-ubuntu-instance`
  `multipass purge`

#### Start/Stop multipass instance
  `multipass {start,stop} my-ubuntu-instance

#### Find alternative instances to launch with multipass
  `multipass find`

#### multipass help
  `multipass help`
