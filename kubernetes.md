## kubernetes

### Commands used for generating certificates for Ansible deployments

#### Generate the certificate
openssl genrsa -out ca.key 2048

openssl req -x509 -new -nodes -key ca.key -subj "/CN=k8s_staging" -days 10000 -out ca.crt


#### Generate cert hash
openssl x509 -pubkey -in /etc/kubernetes/pki/ca.crt | openssl rsa -pubin -outform der 2>/dev/null | openssl dgst -sha256 -hex | sed 's/^.* //'

#### Join command example that shows cert hash format
kubeadm join --discovery-token abcdef.1234567890abcdef --discovery-token-ca-cert-hash sha256:1234..cdef 1.2.3.4:6443

### Raspberry Pi cluster setup steps:

#### Add boot parameters
`sudo vim /boot/cmdline.txt`  
Add: `cgroup_enable=cpuset cgroup_memory=1 cgroup_enable=memory`

#### Disable swap file
`sudo dphys-swapfile swapoff`  
`sudo dphys-swapfile uninstall`  
`sudo apt purge dphys-swapfile`

#### Install Docker
`curl -sSL get.docker.com | sh`
`sudo usermod -aG docker jay`

#### Create daemon.json file
On every node:

`sudo vim /etc/docker/daemon.json`

Contents:

    {
      "exec-opts": ["native.cgroupdriver=systemd"],
      "log-driver": "json-file",
      "log-opts": {
        "max-size": "100m"
      },
      "storage-driver": "overlay2"
    }

Reboot

#### Add Kubernetes repository
On every node:  
`sudo vim /etc/apt/sources.list.d/kubernetes.list`  
Add:  
`deb http://apt.kubernetes.io/ kubernetes-xenial main`

`curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -`
`sudo apt update`

`sudo apt install kubeadm kubectl kubelet`

#### Initialize the pod network
Master-only:

`kubeadm init --pod-network-cidr=10.17.0.0/16 --service-cidr=10.18.0.0/24 --service-dns-domain=home-network.io`
Linux Academy version is shorter:  
`sudo kubeadm init --pod-network-cidr=10.244.0.0/16`

Without declaring a network:  
`sudo kubeadm init --token-ttl=0`

Note: You will get some output that will include the join command, but don't join nodes yet

#### Copy config file to home directory
######On the master:  
`mkdir -p ~.kube`  
`sudo cp /etc/kubernetes/admin.conf ~/.kube/config`  
`sudo chown $(id -u):$(id -g) $HOME/.kube/config`

######Install flannel network driver (lack of sudo is iintentional):
`kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml`

######Make sure all the pods come up:
`kubectl get pods --all-namespaces`

Once they do, run the join command on the other nodes

Verify if the nodes have joined:  
`kubectl get nodes`

#### Reinitialize the pod network
If you run into a problem and need to reinitialize, on all nodes:  
`kubeadm reset`

`rm ~/.kube/config`  
`sudo cp /etc/kubernetes/admin.conf ~/.kube/config`  
`chown jay: .kube/config`  

`kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml`

#### Check logs of a pod

`kubectl logs -n kube-system kube-flannel-ds-arm-sp94q`
