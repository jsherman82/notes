# systemd-resolved

## Show assigned DNS servers:
`systemd-resolve --status |grep DNS\ Servers`

## Flush DNS cache:
`systemd-resolve --flush-caches`

Note: You can also restart the `systemd-resolved` service, which also flushes the cache.

## View statistics:
`systemd-resolve --statistics`

Note: This is one way to find out if the cache is clear

## Disable listening on port 53 (this caused an issue with dnsmasq on pi-hole):
Edit `/etc/systemd/resolved.conf` and add:

`DNSStubListener=no`
