# light-locker

###### Lock the screen:
`dm-tool lock`

###### Configure xflock4 to use light-locker:
`xfconf-query -c xfce4-session -p /general/LockCommand -s "dm-tool lock" --create -t string`
