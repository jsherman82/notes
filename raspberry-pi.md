## Raspberry Pi

### cgroups

#### cgroups for Kubernetes:
cgroup_enable=cpuset cgroup_enable=memory cgroup_memory=1

#### Disable ipv6 via cmdline.txt:
ipv6.disable=1

#### Create compressed image
  `sudo dd bs=4M if=/dev/sdc |gzip > /path/to/image_$(date +%F).gz`

#### Restore compressed image
  `gzip -dc /path/to/image.gz | sudo dd bs=4M of=/dev/sdb`

#### Shrink image
  Download: https://github.com/Drewsif/PiShrink
  `sudo pishrink.sh imagefile.img`

#### Possible fix for fsck timeout during boot on large sd cards
  Not tested, but adding this mount option to "/" might fix it: x-systemd.device-timeout=120

#### Force HDMI every boot
Edit `/boot/config.txt`:

Add these lines:

`#Always force HDMI output and enable HDMI sound`

`hdmi_force_hotplug=1`

`hdmi_drive=2`

#### Fix date and time:
Create symbolic link:

`ln -s /usr/share/zoneinfo/America/Detroit /etc/localtime`
