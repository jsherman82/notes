# AWX

## Installation steps
#### Install requiered packages
    apt install git docker.io nodejs npm python-pip
    
This includes docker prerequisite, which is a replacement for docker-py:
    
    pip install docker-compose

#### Use npm to update npm
    npm install npm --global

#### Clone AWX repo
    git clone https://github.com/ansible/awx.git

    cd awx/installer/

#### Generate secret key
    openssl rand -hex 32

#### Edit inventory file, add/update options
    postgres_data_dir=/var/lib/pgdocker
    host_port=8080
Uncomment: `use_docker_compose=true`

    docker_compose_dir=/var/lib/awx
    pg_password=XvbRqW2ePQw6
    rabbitmq_password=uYN6z8LCu5h6
    rabbitmq_erlang_cookie=cookiemonster
    admin_user=jay
    admin_password=PENyvvUZ9B7C
    secret_key=<generated key>

#### Fix ssl_match_hostname issue in 18.04
    cp -r /usr/local/lib/python2.7/dist-packages/backports/ssl_match_hostname/ /usr/lib/python2.7/dist-packages/backports

#### Run the installer
    ansible-playbook -i inventory install.yml

## Previous issues
#### errors regarding docker.py not being available during build
This could be because the python interpreter in the inventory file is set to python2, set it to python3 instead.
An article mentioned to run the following regarding docker.py, which may or may not be related:

    sudo pip3 install docker-compose==1.25.0