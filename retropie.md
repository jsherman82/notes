# Retropie

## Enable auto-login
This is useful for when Raspbian updates make RetroPie not auto-login.

`sudo raspbi-config`

Choose option 3: Boot Options
Choose option B1: Desktop / CLI
Choose option B2: Console Autologin
Select Finish, and reboot the pi.

## Force HDMI every boot
Edit `/boot/config.txt`
  
Uncomment these lines:

`hdmi_force_hotplug=1`

`hdmi_drive=2`

## Fix "timed out" error when fsck runs on a very large volume
Edit /etc/fstab, add the following mount option to the root filesystem:

`x-systemd.device-timeout=120`

## Steps for setting up a fresh Retropie
  * Set password
  * Copy ssh key
  * Change megadrive branding to genesis:
      Edit/create /opt/retropie/configs/all/platforms.cfg
      Add:
        megadrive_theme="genesis"
        megadrive_platform="genesis"
  * Reinstall the Genesis emulator so it uses the new settings
  * Update retropie, confirm distro package update
  * Install safe shutdown:
      wget -O - "https://raw.githubusercontent.com/crcerror/retroflag-picase/master/install.sh" | sudo bash
  * Choose localization options (set WiFi country, locale, keyboard layout, timezone)
  * Disable overscan
  * Enable SSH
  * Set hostname
  * Install ntp, tmux, vim-nox
  * Edit the /etc/default/openntpd file and add the -s option
  * Enable HDMI fix (above)
  * Also set hdmi_group=1 and hdmi_mode=16
  * Change ls alias to: ls -lhF --time-style=long-iso --color=auto
  * Rename amiga folder so it can't be found
  * Install Syncthing repo key:
      curl -s https://syncthing.net/release-key.txt | sudo apt-key add -
  * Install Syncthing repo:
      echo "deb https://apt.syncthing.net/ syncthing stable" | sudo tee /etc/apt/sources.list.d/syncthing.list
  * Install syncthing
  * Set syncthing to auto-start: sudo systemctl enable syncthing@pi
  * Disable run menu
  * Edit /opt/retropie/configs/all/retroarch.cfg and update save paths to:
      savefile_directory = /home/pi/RetroPie/saves/srm
      savestate_directory = /home/pi/RetroPie/saves/state
  * Also in the retroarch.cfg file, set the save interval, and auto-snapshot options
  * Create the directories from the previous step
  * Download git repo
  * Symlink gamelists and downloaded_images to /opt/retropie/configs/all/emulationstation
  * Copy over BIOS files (or just symlink the repo)
  * Max out the volume
  * Disable UI sounds
  * Enable slide transition when launching games
  * Wipe logs:
      sudo find /var/log/ -type f -name *.xz -exec rm {} \;
      sudo find /var/log/ -type f -name *log.* -exec rm {} \;
      sudo find /var/log/ -type f -exec truncate -s 0 {} \;
  * Delete ssh host keys:
      sudo rm /etc/ssh/ssh_host_*
  * Enable regenerate_ssh_host_keys service:
      sudo systemctl enable regenerate_ssh_host_keys
  * Clean syncthing
      sudo systemctl stop syncthing@pi
      rm -rf ~/.config/syncthing/*
  * Reset machine-id
      sudo truncate -s 0 /etc/machine-id