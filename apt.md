# apt

#### Fix deprecated apt-key errors:
List all current apt keys:

`sudo apt-key list`

Find the last eight characters of the key (two sets of four characters), and construct a command, similar to the ones below:

###### Example commands to migrate keys:
`sudo apt-key export 4E3EFAE4 | sudo gpg --dearmour -o /etc/apt/trusted.gpg.d/lutris.gpg`

`sudo apt-key export 12576482 | sudo gpg --dearmour -o /etc/apt/trusted.gpg.d/cisofy.gpg`

`sudo apt-key export 6E93CD0C | sudo gpg --dearmour -o /etc/apt/trusted.gpg.d/crowdsec.gpg`

`sudo apt-key export 0EBFCD88 | sudo gpg --dearmour -o /etc/apt/trusted.gpg.d/docker.gpg`

`sudo apt-key export CDEF74BB | sudo gpg --dearmour -o /etc/apt/trusted.gpg.d/kernelcare_src.gpg`

###### Related links
https://www.omgubuntu.co.uk/2022/06/fix-apt-key-deprecation-error-on-ubuntu
