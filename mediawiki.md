## MediaWiki

#### Upgrading
Download latest release:

https://www.mediawiki.org/wiki/Download

Move the symbolic link:

`mv /var/lib/html/mediawiki /var/lib/html/mediawiki.previous`

Move the new version of Mediawiki to /var/lib:

`tar -xvf mediawiki-version.tar.gz`

`chown www-data -R mediawiki-dir`

`mv mediawiki-dir /var/lib`

Copy important files from the old release directory to the new one:
`cp -prv LocalSettings.php images /var/lib/mediawiki-new-version`

Note: The page broke as soon as it was pointed to the new version, I had to copy two skin folders to the new version.

#### Applying CSS
I edited the following file to change CSS globally:

https://wiki.learnlinux.tv/index.php/MediaWiki:Common.css

I used CSS from here for the current theme (Timeless):

https://meta.wikimedia.org/wiki/User:Aron_Manning/skin-theming.css?action=raw&ctype=text/css