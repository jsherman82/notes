# Thunderbird

## Account setup
#### Initial setup
* Add Google account, using app password
  * `imap.gmail.com` 993 SSL/TLS normal password
  * `smtp.gmail.com` 465 SSL/TLS normal password
* Add fastmail account, using app password
  * `imap.fastmail.com` 993 SSL/TLS normal password
  * `smtp.fastmail.com` 465 SSL/TLS normal password
* Disable html e-mail for both
* Add signature
* Set the below options

## Tweaks
#### Make text in compose window wrap to window width:
In the configuration editor, change:

`mailnews.wraplength = 0`

#### Change default sort order
In the configuration editor, change:

`mailnews.default_sort_order = 2`
