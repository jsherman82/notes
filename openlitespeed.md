# openlitespeed

## Past issues and solutions
#### Fix openlitespeed not being restarted after cert renewal:
Edit `/usr/lib/systemd/system/certbot.service`
Change the following line:
`ExecStart=/usr/bin/certbot -q renew`
To:
`ExecStart=/usr/bin/certbot -q renew --deploy-hook 'systemctl restart lsws'`
