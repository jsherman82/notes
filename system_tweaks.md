# System Tweaks

## Set default audio device
Run: `pactl list short sinks`

Set the default with: `pactl set-default-sink <your_device_name>`

Add it to startup somehow to try and fix it.

From: https://askubuntu.com/questions/1038490/how-do-you-set-a-default-audio-output-device-in-ubuntu-18-04

## Another possibility
Edit `/etc/pulse/default.pa`

Comment out:
`load-module module-switch-on-connect`

## Steam launch options for offloading to nvidia
`__NV_PRIME_RENDER_OFFLOAD=1 __GLX_VENDOR_LIBRARY_NAME=nvidia`

`__VK_LAYER_NV_optimus=NVIDIA_only %command%`

From: https://github.com/Askannz/optimus-manager/issues/171
