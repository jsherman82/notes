# Nextcloud

## Installation Notes

#### Required packages (Ubuntu 20.04)
    apache2 libapache2-mod-php7.4 php7.4-xml php7.4-cgi php7.4-mysql php7.4-mbstring php7.4-gd php7.4-curl php7.4-zip

#### Required apache modules
  * env
  * dir
  * headers
  * mime
  * php7.2
  * proxy
  * proxy_http
  * proxy_wstunnel
  * rewrite
  * ssl

#### hsts settings in Apache
Add the following config to the SSL config file for Apache:

    <IfModule mod_headers.c>
      Header always set Strict-Transport-Security "max-age=15552000; includeSubDomains"
    </IfModule>

#### Setting up cron
Edit crontab as www-data user:

    sudo crontab -u www-data -e

Add:

    */5  *  *  *  * php -f /var/www/nextcloud.learnlinux.tv/cron.php

Note: The `www-data` user must be the owner of the config.php file, or this won't work.

#### Security scanner
There's a security scanner available, that can recommend some security improvements:

https://scan.nextcloud.com

#### Installing Collabora Office
  * `a2enmod proxy`
  * `a2enmod proxy_wstunnel`
  * `a2enmod proxy_http`
  * `apt-get install docker.io`
  * `docker run -t -d -p 127.0.0.1:9980:9980 -e 'domain=nextcloud\\.mydomain\\.net' --restart always --cap-add MKNOD collabora/code`
  * Add virtual host file (below)
  * `sudo certbot --installer apache --authenticator webroot -d office.mydomain.net`
     Note: When the command asks for document root, give it the existing nextcloud document root

#### Virtual host file for collabora:

    <VirtualHost *:443>
      ServerName office.mydomain.net:443

      # Encoded slashes need to be allowed
      AllowEncodedSlashes NoDecode

      # keep the host
      ProxyPreserveHost On

      # static html, js, images, etc. served from loolwsd
      # loleaflet is the client part of LibreOffice Online
      ProxyPass           /loleaflet https://127.0.0.1:9980/loleaflet retry=0
      ProxyPassReverse    /loleaflet https://127.0.0.1:9980/loleaflet

      # WOPI discovery URL
      ProxyPass           /hosting/discovery https://127.0.0.1:9980/hosting/discovery retry=0
      ProxyPassReverse    /hosting/discovery https://127.0.0.1:9980/hosting/discovery

      # Main websocket
      ProxyPassMatch "/lool/(.*)/ws$" wss://127.0.0.1:9980/lool/$1/ws nocanon

      # Admin Console websocket
      ProxyPass   /lool/adminws wss://127.0.0.1:9980/lool/adminws

      # Download as, Fullscreen presentation and Image upload operations
      ProxyPass           /lool https://127.0.0.1:9980/lool
      ProxyPassReverse    /lool https://127.0.0.1:9980/lool

      # Container uses a unique non-signed certificate
      SSLProxyEngine On
      SSLProxyVerify None
      SSLProxyCheckPeerCN Off
      SSLProxyCheckPeerName Off

    </VirtualHost>

## Previous Issues

#### "Authorization Failed" error while using Synology Cloud Sync
Add /remote.php/webdav to the URL

#### "Request entity too large" error when uploading files
Edit the nginx config, and in the server { section, place the following:

    client_max_body_size        10G;
    client_body_buffer_size     400M;

#### e-book reader not saving bookmarks
    * Add the following lines to /var/www/nextcloud.domain.net/config/config.php:
        'overwrite.cli.url' => 'https://nextcloud.domain.net',
        'htaccess.RewriteBase' => '/',
    * Run the following command:
        `sudo -u www-data php /var/www/nextcloud.domain.net/occ maintenance:update:htaccess`

#### Difficulties syncing contacts/calendar behind proxy
Some combination of the below worked.
Added the following configuration items to config.php:

    'overwrite.cli.url' => 'https://cloud.home-network.io',
    'overwritehost' => 'cloud.home-network.com',
    'overwriteprotocol' => 'https',
    'htaccess.RewriteBase' => '/',
    "trusted_proxies" => ['172.16.249.4'],
    "overwritehost" => "cloud.home-network.io",

Followed the following article (namely enabling remoteip mod in apache, followed the configuration):

    https://www.techandme.se/set-up-nginx-reverse-proxy/
  Used the following URL for both calendar/contacts (auto-discovery worked in MacOS but not iOS):
  
    https://cloud.mydomain.com/remote.php/dav/principals/users/jay

#### URL for syncing contacts to macOS
Use the following URL for adding contacts to macOS:
  `https://cloud.mydomain.com/remote.php/dav/principals/users/jay`

####  memcache errors in config page
  * install: `php-apcu`
  * Edit: `/var/www/nextcloud/config/config.php`
     Add: `'memcache.local' => '\OC\Memcache\APCu'`
