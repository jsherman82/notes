# Firefox

## Keyboard shortcuts
* Close tab: CTRL+W
* Close window: CTRL+Q
* Move focus to address bar: CTRL+L

## about:config tweaks

#### Increase mouse-wheel scroll speed
    mousewheel.min_line_scroll_amount = 60
Note: I might have used 75.

#### Lower session store interval
Update `browser.sessionstore.interval` to a larger number

#### Disable site notifications
    dom.webnotifications.enabled = false

#### Closing last tab closes window
    browser.tabs.closeWindowWithLastTab true

#### Enable 1080p video support:
    media.mediasource.enabled from = true
    media.mediasource.webm.enabled = true
    media.mediasource.mp4.enabled = true
    media.fragmented-mp4.use-blank-decoder = false
    media.fragmented-mp4.* = true
    media.mediasource.ignore_codecs = true

#### Stop sites from disabling right-click:
    dom.event.clipboardevents.enabled = false
