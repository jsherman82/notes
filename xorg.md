# Xorg

#### Start an additional X session:
In a new TTY, type: `startx -- :1`

#### Enabling natural scrolling (for environments that can't do so through settings):
In the /usr/share/X11/xorg.conf.d/ directory, create the two files below:

##### 20-natural-scrolling-touchpad.conf
	Section "InputClass"
	    Identifier "Natural Scrolling Touchpads"
	    MatchIsTouchpad "on"
	    MatchDevicePath "/dev/input/event*"
	    Option "VertScrollDelta" "-111"
	    Option "HorizScrollDelta" "-111"
	EndSection

##### 20-natural-scrolling-mice.conf
	Section "InputClass"
	    Identifier "Natural Scrolling Mice"
	    MatchIsPointer "on"
	    MatchIsTouchpad "off"
	    MatchDevicePath "/dev/input/event*"
	    Option "VertScrollDelta" "-1"
	    Option "HorizScrollDelta" "-1"
	    Option "DialDelta" "-1"
	EndSection

##### Previous issues:
According to the bug report (bug 1634449) occassional screen blackouts can be solved via (untested):

`sudo cp /usr/share/doc/xserver-xorg-video-intel/xorg.conf /etc/X11/`
