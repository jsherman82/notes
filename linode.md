# Linode

#### Clone a Linode disk to a local backup
* Shut down the Linode, then start it in recovery mode.
* Set a root password: `passwd`
* Start the ssh service: `service ssh start`

Create the backup:

`ssh root@<LINODE_IP> "dd if=/dev/sda " | dd of=/local/path/to/linode.img`

#### Mount a backed up disk image

`mkdir linode`

`mount -o loop linode.img linode`
