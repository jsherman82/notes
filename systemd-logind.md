# systemd-logind

## Fix suspend when lid closed an plugged into power
#### Analyzing system state:
Edit `/etc/systemd/logind.conf`, and change:

`HandleLidSwitchExternalPower=ignore`

