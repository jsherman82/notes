# truenas

## Volumes
#### List volumes/snapshots and their size
`zfs list -o space -r volume1`

Note: That may not show snapshots. If not, enable with:

`zpool set listsnapshots=on rpool`

#### Format an external disk as UFS
`sudo newfs /dev/da0p1`

## Snapshots
#### Clean older snapshots
List snapshots with:
  'zfs list -H -t snapshot -o name -S creation -r <dataset name> | tail -10'

Actually remove the snapshots:
  'zfs list -H -t snapshot -o name -S creation -r <dataset name> | tail -10 | xargs -n 1 zfs destroy'

From: https://techblog.jeppson.org/2018/03/zfs-delete-oldest-n-snapshots/

#### Clean older snapshots (previous method I've used)
`zfs list -t snapshot -o name -S creation | grep ^volume1/proxmox | tail -n +16 | xargs -n 1 zfs destroy -vr`

Test with:

`zfs list -t snapshot -o name -S creation | grep ^volume1/proxmox | tail -n +16 | xargs -n 1 echo`

## ACL
#### Remove ACL permissions
`find directory/ | setfacl -b`

## Jails
#### Update plex jail
`service plexmediaserver stop`
`pkg update && pkg upgrade multimedia/plexmediaserver`
`service plexmediaserver start`

## Helpful links:
Setting up Plex jail:
https://forums.freenas.org/index.php?threads/tutorial-how-to-install-plex-in-a-freenas-11-0-jail.19412/
