# autofs

## Examples

#### auto.master example for NFS:
    /home/jay/storage /etc/auto.nfs --ghost

#### auto.nfs example for NFS:
    home -fstype=nfs4,rw storage.home-network.io:/mnt/volume1/home/jay

#### Debugging Auto Mount Problems
    systemctl stop autofs
    automount -f -v
